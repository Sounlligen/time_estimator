#-------------------------------------------------
#
# Project created by QtCreator 2016-07-07T09:25:18
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = time_estimator
TEMPLATE = app


SOURCES += src/main.cpp\
        src/mainwindow.cpp \
    src/ctimeestimator.cpp \
    src/cdialog.cpp \
    src/crobot.cpp \
    src/caction.cpp \
    src/cspeedramp.cpp \
    src/cevent.cpp \
    src/cabstractpart.cpp \
    src/cewait.cpp \
    src/ceobstacle.cpp \
    src/castraigthline.cpp \
    src/caturn.cpp \
    src/caloading.cpp \
    src/cadvancedobstacles.cpp

HEADERS  += headers/mainwindow.h \
    headers/ctimeestimator.h \
    headers/cdialog.h \
    headers/crobot.h \
    headers/caction.h \
    headers/cspeedramp.h \
    headers/cevent.h \
    headers/cabstractpart.h \
    headers/cewait.h \
    headers/ceobstacle.h \
    headers/castraigthline.h \
    headers/caturn.h \
    headers/caloading.h \
    headers/cadvancedobstacles.h

FORMS    += ui/mainwindow.ui \
    ui/cdialog.ui \
    ui/cadvancedobstacles.ui
