#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTableWidget>
#include <QFile>
#include <QMessageBox>
#include <QTextStream>
#include <QFileDialog>
#include <cmath>
#include "cdialog.h"
#include "cadvancedobstacles.h"
#include "ctimeestimator.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_pushButton_dod_odc_clicked();
    void on_pushButton_ok_dl_odc_clicked();
    void on_pushButton_dod_zak_clicked();
    void on_pushButton_ok_pr_zak_clicked();
    void on_pushButton_dod_zat_clicked();
    void on_pushButton_ok_czas_zat_clicked();
    void on_pushButton_next_clicked();
    void on_pushButton_dod_prze_clicked();
    void on_pushButton_ok_pr_prze_clicked(); 
    void on_pushButton_dod_load_clicked();
    void on_pushButton_ok_time_load_clicked();
    void on_pushButton_add_to_path_clicked();
    void on_pushButton_delete_from_path_clicked();
    void on_pushButton_delete_from_event_clicked();
    void on_pushButton_clear_path_clicked();
    void on_pushButton_clear_events_clicked();
    void on_pushButton_advanced_obstacles_clicked();

    void saveTableToFile();
    void loadTableFromFile();

private:
    Ui::MainWindow *ui;
    CTimeEstimator time_estimator;

    std::vector<std::pair<QString, QString> > names_of_actions;
    std::vector<std::pair<QString, QString> > names_of_events;

    QMessageBox msg_box;

    void updateTableTrajectory();
    void updateTableEvents();
    void setTableElementNum(int row, int column, float value, QString unit, QTableWidget *name_of_table);
    void setTableElementStr(int row, int column, QString nazwa, QTableWidget *name_of_table);

    void clearTable(QTableWidget *name_of_table);

    void countersUpdate();
    void countParts();

    int number_of_action_row;

    int lines, turns, loadings, waits, obstacles;
};

#endif // MAINWINDOW_H
