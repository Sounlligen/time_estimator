#ifndef CEVENT_H
#define CEVENT_H

#include <vector>
#include "cabstractpart.h"

enum TypesOfEvents {Wait, Obstacle};

class CEvent : public CAbstractPart
{
public:
    CEvent(double value_of_event_, CRobot *robot_);

private:
};

#endif // CEVENT_H
