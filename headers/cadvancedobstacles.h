#ifndef CADVANCEDOBSTACLES_H
#define CADVANCEDOBSTACLES_H

#include <QDialog>

namespace Ui {
class CAdvancedObstacles;
}

class CAdvancedObstacles : public QDialog
{
    Q_OBJECT

public:
    explicit CAdvancedObstacles(QWidget *parent = 0);
    ~CAdvancedObstacles();

    double getDimension1() const;
    double getDimension2() const;
    QString getShape() const;

    void setDimension1(double value);
    void setDimension2(double value);
    void setShape(const QString &value);

private slots:
    void on_comboBox_obstacle_shape_currentIndexChanged(const QString &arg1);

    void on_buttonBox_accepted();

private:
    Ui::CAdvancedObstacles *ui;

    double dimension1, dimension2;
    QString shape;
};

#endif // CADVANCEDOBSTACLES_H
