#ifndef CABSTRACTPART_H
#define CABSTRACTPART_H

#include "crobot.h"


class CAbstractPart
{
public:
    CAbstractPart(CRobot *robot_, double value);

    virtual double countTimeOfPart() = 0;

    void setValueOfPart(double value);

    double getValueOfPart() const;


protected:
    CRobot *robot;

    double value_of_part;
};

#endif // CABSTRACTPART_H
