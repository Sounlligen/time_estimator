#ifndef CALOADING_H
#define CALOADING_H

#include "caction.h"

class CALoading : public CAction
{
public:
    CALoading(CRobot *robot_, double value, double load_);

    double countTimeOfPart();

    double getLoad();

private:
    double load;
};

#endif // CALOADING_H
