#ifndef CDIALOG_H
#define CDIALOG_H

#include <QDialog>
#include <QPainter>
#include <QPen>
#include <vector>
#include "ctimeestimator.h"

enum Directions {Right, Down, Left, Up};

namespace Ui {
class CDialog;
}

class CDialog : public QDialog
{
    Q_OBJECT

public:
    explicit CDialog(QWidget *parent = 0);

    void setTimeValue(double time);

    void setActionsToVisualization(vector<CAction *> value);

    void paint();

    ~CDialog();

private slots:
    void on_pushButton_close_clicked();

private:
    Ui::CDialog *ui;

    QPixmap *pixmapVisualization;
    QPainter *painterVisualization;

    vector<CAction *> actions_to_vis;

    vector <QPoint> points;
    vector <QPoint> loadings;

    int factor, xp, yp;
};

#endif // CDIALOG_H
