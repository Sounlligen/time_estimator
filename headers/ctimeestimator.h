#ifndef CTIMEESTIMATOR_H
#define CTIMEESTIMATOR_H

#include <vector>
#include <cmath>
#include "crobot.h"
#include "castraigthline.h"
#include "caturn.h"
#include "caloading.h"
#include "ceobstacle.h"
#include "cewait.h"

using std::vector;
using std::pair;

//All number are expressed in SI units

class CTimeEstimator
{
public:
    CTimeEstimator();

    double getPathTime();
    void clearActions();
    void clearEvents();

    void addALineLength(double length, int position = -1);
    void addAArcRadius(double cor_in_w, double cor_out_w, bool is_right = true, int position = -1);
    void addALoadingTime(double time, double load, int position = -1);

    void deleteAction(int position = -1);
    void deleteEvent(int position = -1);

    void addEWaitTime(double time);
    void addEObstacleRadius(double radius);

    static int checkTypeOfAction(CAction *action);
    static int checkTypeOfEvent(CEvent *event);

    vector<CAction *> getActions();
    vector<CEvent *> getEvents();

    CRobot getRobot();

private:
    double countActionsTime();
    double countEventsTime();

    void addStraigthOrTurn(int type, double value, bool is_right = true, int position = -1, double corridor_in = -1, double corrdor_out = -1);

    CRobot robot;
    vector<CAction *> actions;
    vector<CEvent *> events;
};

#endif // CTIMEESTIMATOR_H
