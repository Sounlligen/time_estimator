#ifndef CASTRAIGTHLINE_H
#define CASTRAIGTHLINE_H

#include "caction.h"

class CAStraigthLine : public CAction
{
public:
    CAStraigthLine(CRobot *robot_, double value, int movement_type);

    double countTimeOfPart();
};

#endif // CASTRAIGTHLINE_H
