#ifndef CATURN_H
#define CATURN_H

#include <cmath>
#include "caction.h"

class CATurn : public CAction
{
public:
    CATurn(CRobot *robot_, double value, int movement_type, bool is_right = true);
    CATurn(CRobot *robot_, double corridor_in, double corridor_out, int movement_type, bool is_right = true);

    double countTimeOfPart();

    double getCorridorIn() const;
    double getCorridorOut() const;
    bool getRight() const;


private:
    double countRadiusFromCorridors(double corridor_in, double corridor_out);

    double corr_in;
    double corr_out;

    bool right;
};

#endif // CATURN_H
