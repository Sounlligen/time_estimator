#ifndef CROBOT_H
#define CROBOT_H
#include "cspeedramp.h"

//All number are expressed in SI units

class CRobot
{
public:
    CRobot();

    void setParams(double small_obstacle_radius_, double avoidance_offset_);

    void setAvoidanceEntryRadius(double value);
    void setAvoidanceOffset(double value);
    void setIsLoaded(bool value);

    double getRobotWidth() const;
    double getAvoidanceEntryRadius() const;
    double getAvoidanceOffset() const;
    bool getIsLoaded() const;

    CSpeedRamp *getSpeedRamp() const;    

private:
    CSpeedRamp *speed_ramp;

    bool is_loaded;

    double robot_width;
    //Radius for beginning and end of avoiding obstacle
    double avoidance_entry_radius;
    //The distance between robot and obstacle when avoiding
    double avoidance_offset;
};

#endif // CROBOT_H
