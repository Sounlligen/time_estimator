#ifndef CACTION_H
#define CACTION_H

#include <vector>
#include "cabstractpart.h"

//Enum used to describe if robot starts or stops at the beginning of movement
enum TypesOfMovement {NoStartStop, Start, Stop, StartStop, StartStopAlone, None};
enum TypesOfActions {StraigthLine, Turn, Loading};

class CAction : public CAbstractPart
{
public:
    CAction(CRobot *robot, double value, int movement_type);

    void addStartOrStop(int value);
    void removeStartOrStop(int value);
    void removeStartOrStop();

    void setTypeOfMovement(int value);

    int getTypeOfMovement() const;

protected:
    double countOnePartOfPath(double part_length, double radius = -1.0);

    int type_of_movement;

};

#endif // CACTION_H
