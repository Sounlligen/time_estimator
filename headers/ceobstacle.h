#ifndef CEOBSTACLE_H
#define CEOBSTACLE_H

#include <cmath>
#include "cevent.h"

class CEObstacle : public CEvent
{
public:
    CEObstacle(double value_of_event_, CRobot *robot_);

    virtual double countTimeOfPart();
};

#endif // CEOBSTACLE_H
