#ifndef CEWAIT_H
#define CEWAIT_H
#include "cevent.h"

class CEWait : public CEvent
{
public:
    CEWait(double value_of_event_, CRobot *robot_);

    virtual double countTimeOfPart();
};

#endif // CEWAIT_H
