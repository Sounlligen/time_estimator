#ifndef CSPEEDRAMP_H
#define CSPEEDRAMP_H


class CSpeedRamp
{
public:
    CSpeedRamp(double max_speed_, double acc_time_, double acc_dist_, double decc_time_, double decc_dist_);

    double getMaxSpeed() const;
    double getArcMaxSpeed() const;
    double getAccTime() const;
    double getAccDist() const;
    double getDeccTime() const;
    double getDeccDist() const;
    double getDeccAccDistMaxSpeed() const;

    void countArcMaxSpeed(double robot_width, double radius);
    void setMaxSpeed(double value);
    void setAccTime(double value);
    void setAccDist(double value);
    void setDeccTime(double value);
    void setDeccDist(double value);    

private:
    void setDeccAccDistMaxSpeed();

    double max_speed;
    double arc_max_speed;

    double acc_time;
    double acc_dist;

    double decc_time;
    double decc_dist;

    double decc_acc_dist_max_speed;
};

#endif // CSPEEDRAMP_H
