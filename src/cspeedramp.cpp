#include "headers/cspeedramp.h"

CSpeedRamp::CSpeedRamp(double max_speed_, double acc_time_, double acc_dist_, double decc_time_, double decc_dist_)
{
    max_speed = max_speed_;

    acc_time = acc_time_;
    acc_dist = acc_dist_;

    decc_time = decc_time_;
    decc_dist = decc_dist_;

    setDeccAccDistMaxSpeed();
}

double CSpeedRamp::getMaxSpeed() const
{
    return max_speed;
}

double CSpeedRamp::getArcMaxSpeed() const
{
    return arc_max_speed;
}

double CSpeedRamp::getAccTime() const
{
    return acc_time;
}

void CSpeedRamp::setMaxSpeed(double value)
{
    max_speed = value;
    setDeccAccDistMaxSpeed();
}



void CSpeedRamp::setAccTime(double value)
{
    acc_time = value;
}

double CSpeedRamp::getAccDist() const
{
    return acc_dist;
}

double CSpeedRamp::getDeccDist() const
{
    return decc_dist;
}
double CSpeedRamp::getDeccAccDistMaxSpeed() const
{
    return decc_acc_dist_max_speed;
}

void CSpeedRamp::countArcMaxSpeed(double robot_width, double radius)
{
    arc_max_speed = max_speed/(1 + robot_width/(2*radius));
}

void CSpeedRamp::setAccDist(double value)
{
    acc_dist = value;
    setDeccAccDistMaxSpeed();
}

double CSpeedRamp::getDeccTime() const
{
    return decc_time;
}

void CSpeedRamp::setDeccTime(double value)
{
    decc_time = value;
}

void CSpeedRamp::setDeccDist(double value)
{
    decc_dist = value;
    setDeccAccDistMaxSpeed();
}

void CSpeedRamp::setDeccAccDistMaxSpeed()
{
    decc_acc_dist_max_speed = (decc_dist + acc_dist)/max_speed;
}
