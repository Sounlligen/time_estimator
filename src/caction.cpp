#include "headers/caction.h"

CAction::CAction(CRobot *robot, double value, int movement_type) : CAbstractPart(robot, value)
{
    type_of_movement = movement_type;
}

void CAction::addStartOrStop(int value)
{
    if(type_of_movement == NoStartStop)
    {
        type_of_movement = value;
    }
    else
    {
        if(type_of_movement == Start)
        {
            if(value == Stop)
            {
                type_of_movement = StartStop;
            }
        }

        if(type_of_movement == Stop)
        {
            if(value == Start)
            {
                type_of_movement = StartStop;
            }
        }
    }
}

void CAction::removeStartOrStop(int value)
{
    if(value == Start)
    {
        if(type_of_movement == StartStop)
        {
            type_of_movement = Stop;
        }
        if(type_of_movement == Start)
        {
            type_of_movement = NoStartStop;
        }
    }

    if(value == Stop)
    {
        if(type_of_movement == StartStop)
        {
            type_of_movement = Start;
        }
        if(type_of_movement == Stop)
        {
            type_of_movement = NoStartStop;
        }
    }
}

void CAction::removeStartOrStop()
{
    type_of_movement = NoStartStop;
}

void CAction::setTypeOfMovement(int value)
{
    type_of_movement = value;
}

int CAction::getTypeOfMovement() const
{
    return type_of_movement;
}

double CAction::countOnePartOfPath(double part_length, double radius)
{
    double acc_time = 0;
    double acc_dist = 0;

    double decc_time = 0;
    double decc_dist = 0;

    double max_speed = robot->getSpeedRamp()->getMaxSpeed();
    if(radius > 0)
    {
        robot->getSpeedRamp()->countArcMaxSpeed(robot->getRobotWidth(), radius);
        max_speed = robot->getSpeedRamp()->getArcMaxSpeed();
    }

    switch(type_of_movement)
    {
    case NoStartStop: break;

    case Start: acc_time = robot->getSpeedRamp()->getAccTime();
                acc_dist = robot->getSpeedRamp()->getAccDist();
        break;

    case Stop:  decc_time = robot->getSpeedRamp()->getDeccTime();
                decc_dist = robot->getSpeedRamp()->getDeccDist();
        break;

    case StartStop: acc_time = robot->getSpeedRamp()->getAccTime();
                    acc_dist = robot->getSpeedRamp()->getAccDist();

                    decc_time = robot->getSpeedRamp()->getDeccTime();
                    decc_dist = robot->getSpeedRamp()->getDeccDist();
        break;

    default: return 0;
    }

    double max_speed_time;

    max_speed_time = (part_length - acc_dist - decc_dist)/max_speed;

    return acc_time + max_speed_time + decc_time;
}
