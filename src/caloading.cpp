#include "headers/caloading.h"

CALoading::CALoading(CRobot *robot_, double value, double load_) : CAction(robot_, value, None)
{
    load = load_;
}

double CALoading::countTimeOfPart()
{
    if(load < 0)
    {
        robot->setIsLoaded(false);
    }
    else
    {
        robot->setIsLoaded(true);
    }

    return value_of_part;
}

double CALoading::getLoad()
{
    return load;
}
