#include "headers/caturn.h"

CATurn::CATurn(CRobot *robot_, double value, int movement_type, bool is_right) :
    CAction(robot_, value, movement_type)
{
    right = is_right;
}

CATurn::CATurn(CRobot *robot_, double corridor_in, double corridor_out, int movement_type, bool is_right) :
    CAction(robot_, countRadiusFromCorridors(corridor_in, corridor_out), movement_type)
{
    corr_in = corridor_in;
    corr_out = corridor_out;

    right = is_right;
}

double CATurn::countTimeOfPart()
{
    double angle = M_PI/2;
    double angle_length = angle*value_of_part;

    return countOnePartOfPath(angle_length, value_of_part) - 2*value_of_part/robot->getSpeedRamp()->getMaxSpeed();
}

bool CATurn::getRight() const
{
    return right;
}

double CATurn::countRadiusFromCorridors(double corridor_in, double corridor_out)
{
    return (corridor_in + corridor_out)/2;
}

double CATurn::getCorridorIn() const
{
    return corr_in;
}

double CATurn::getCorridorOut() const
{
    return corr_out;
}
