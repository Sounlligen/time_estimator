#include "headers/mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{    
    ui->setupUi(this);
    this->setWindowTitle("Estimator");

    ui->pushButton_ok_dl_odc->setEnabled(false);
    ui->pushButton_ok_pr_zak->setEnabled(false);
    ui->pushButton_ok_time_load->setEnabled(false);
    ui->pushButton_ok_czas_zat->setEnabled(false);
    ui->pushButton_ok_pr_prze->setEnabled(false);
    ui->pushButton_advanced_obstacles->setEnabled(false);

    ui->spinBox_length_line->setEnabled(false);
    ui->spinBox_cor_in->setEnabled(false);
    ui->SpinBox_cor_out->setEnabled(false);
    ui->spinBox_time_load->setEnabled(false);
    ui->spinBox_time_stop->setEnabled(false);

    ui->comboBox_right_left->setEnabled(false);
    ui->comboBox_load_unload->setEnabled(false);
    ui->comboBox_obstacle_size->setEnabled(false);
    ui->comboBox_obstacle_size->setCurrentIndex(1);

    countersUpdate();

    names_of_actions.push_back(std::make_pair("Straight - length", " m"));
    names_of_actions.push_back(std::make_pair("Turn - radius", " m"));
    names_of_actions.push_back(std::make_pair("Loading - time", " s"));

    names_of_events.push_back(std::make_pair("Stop - time", " s"));
    names_of_events.push_back(std::make_pair("Obstacle - radius", " m"));

    number_of_action_row = -1;

    ui->table_trajectory->setColumnWidth(0,150);
    ui->table_trajectory->setColumnWidth(1,60);
    ui->table_trajectory->setColumnWidth(2,90);

    ui->table_events->setColumnWidth(0,150);
    ui->table_events->setColumnWidth(1,120);

    connect(ui->actionLoad_file, &QAction::triggered, this, &MainWindow::loadTableFromFile);
    connect(ui->actionSave_as, &QAction::triggered, this, &MainWindow::saveTableToFile);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::setTableElementNum(int row, int column, float value, QString unit, QTableWidget *name_of_table)
{
    QTableWidgetItem *tmp = new QTableWidgetItem;
    tmp->setText(QString::number(value, 'f', 2) + unit);
    name_of_table->setItem(row, column, tmp);
}

void MainWindow::setTableElementStr(int row, int column, QString nazwa, QTableWidget *name_of_table)
{
    QTableWidgetItem *tmp = new QTableWidgetItem;
    tmp->setText(nazwa);
    name_of_table->setItem(row, column, tmp);
}

void MainWindow::clearTable(QTableWidget *name_of_table)
{
    int size = name_of_table->rowCount();

    for(int i = 0; i < size; i++)
    {
        name_of_table->removeRow(0);
    }

    countersUpdate();
}

void MainWindow::updateTableTrajectory()
{
    clearTable(ui->table_trajectory);

    vector<CAction *> actions = time_estimator.getActions();

    int row = 0;

    for(unsigned i = 0; i < actions.size(); i++)
    {
        row = i;
        QString type;
        QString value;

        int action_type = CTimeEstimator::checkTypeOfAction(actions.at(i));

        if(action_type == StraigthLine)
        {
            type = "-";
            value = QString::number(actions.at(i)->getValueOfPart(), 'f', 2);
        }
        if(action_type == Turn)
        {
            CATurn *turn = dynamic_cast<CATurn *>(actions.at(i));

            if(turn->getRight())
            {
                type = "Right";
            }
            else
            {
                type = "Left";
            }

            value = QString::number(turn->getCorridorOut(), 'f', 2) + "/" + QString::number(turn->getCorridorIn(), 'f', 2);
        }
        if(action_type == Loading)
        {
            CALoading *load = dynamic_cast<CALoading *>(actions.at(i));

            if(load->getLoad()<0)
            {
                type = "Unload";
            }
            else
            {
                type = "Load";
            }

            value = QString::number(actions.at(i)->getValueOfPart(), 'f', 2);
        }

        value += names_of_actions.at(CTimeEstimator::checkTypeOfAction(actions.at(row))).second;

        ui->table_trajectory->insertRow(row);
        setTableElementStr(row,0,names_of_actions.at(CTimeEstimator::checkTypeOfAction(actions.at(row))).first,ui->table_trajectory);
        setTableElementStr(row,1,type,ui->table_trajectory);
        setTableElementStr(row,2,value,ui->table_trajectory);
    }
}

void MainWindow::updateTableEvents()
{
    clearTable(ui->table_events);

    vector<CEvent *> events = time_estimator.getEvents();

    for(unsigned i = 0; i < events.size(); i++)
    {
        //int row = events.size()-1;
        int row = i;

        ui->table_events->insertRow(row);
        setTableElementStr(row,0,names_of_events.at(CTimeEstimator::checkTypeOfEvent(events.at(row))).first,ui->table_events);
        setTableElementNum(row,1,events.at(row)->getValueOfPart(),names_of_events.at(CTimeEstimator::checkTypeOfEvent(events.at(row))).second,ui->table_events);
    }
}

void MainWindow::countersUpdate()
{
    countParts();

    ui->lcdNumber_licz_odc->display(lines);
    ui->lcdNumber_licz_zak->display(turns);
    ui->lcdNumber_num_loads->display(loadings);
    ui->lcdNumber_licz_zat->display(waits);
    ui->lcdNumber_licz_prze->display(obstacles);
}

void MainWindow::countParts()
{
    lines = turns = loadings = waits = obstacles = 0;

    for(unsigned i = 0; i < time_estimator.getActions().size(); i++)
    {
        int action = CTimeEstimator::checkTypeOfAction(time_estimator.getActions().at(i));

        if(action == StraigthLine) lines++;
        if(action == Turn) turns++;
        if(action == Loading) loadings++;
    }

    for(unsigned i = 0; i < time_estimator.getEvents().size(); i++)
    {
        int event = CTimeEstimator::checkTypeOfEvent(time_estimator.getEvents().at(i));

        if(event == Wait) waits++;
        if(event == Obstacle) obstacles++;
    }
}

void MainWindow::saveTableToFile()
{
    QString filename = QFileDialog::getSaveFileName(this, tr("Save Path"), "/home", tr("Text files (*.txt)"));

    filename += ".txt";

    QFile file(filename);

    if(!file.open(QIODevice::WriteOnly | QIODevice::Text))
    {
        msg_box.setIcon(QMessageBox::Critical);
        msg_box.setText("Could not save file. Maybe you don't have permission?");
        msg_box.exec();

        return;
    }

    QTextStream out(&file);

    out.setFieldWidth(20);
    out.setFieldAlignment(QTextStream::AlignLeft);

    out<<"ACTIONS";

    out.setFieldWidth(0);
    out<<endl;
    out.setFieldWidth(20);

    for(int i = 0; i < ui->table_trajectory->columnCount(); i++)
    {
        out<<ui->table_trajectory->horizontalHeaderItem(i)->text()+",";
    }

    out.setFieldWidth(0);
    out<<endl;
    out.setFieldWidth(20);

    for(int i = 0; i < ui->table_trajectory->rowCount(); i++)
    {
        for(int j = 0; j < ui->table_trajectory->columnCount(); j++)
        {
            if(j == ui->table_trajectory->columnCount() -1)
            {
                QString str = ui->table_trajectory->item(i, j)->text();
                QStringList list = str.split(" ");

                out<<list.at(0) + "," + list.at(1) + ",";
            }
            else
            {
                out<<ui->table_trajectory->item(i, j)->text() + ",";
            }
        }

        out.setFieldWidth(0);
        out<<endl;
        out.setFieldWidth(20);
    }


    out<<"EVENTS";

    out.setFieldWidth(0);
    out<<endl;
    out.setFieldWidth(20);

    for(int i = 0; i < ui->table_events->columnCount(); i++)
    {
        out<<ui->table_events->horizontalHeaderItem(i)->text()+",";
    }

    out.setFieldWidth(0);
    out<<endl;
    out.setFieldWidth(20);

    for(int i = 0; i < ui->table_events->rowCount(); i++)
    {
        for(int j = 0; j < ui->table_events->columnCount(); j++)
        {
            if(j == ui->table_events->columnCount() -1)
            {
                QString str = ui->table_events->item(i, j)->text();
                QStringList list = str.split(" ");

                out<<list.at(0) + "," + list.at(1) + ",";
            }
            else
            {
                out<<ui->table_events->item(i, j)->text() + ",";
            }
        }

        out.setFieldWidth(0);
        out<<endl;
        out.setFieldWidth(20);
    }

    out<<"END OF PATH";

    out.setFieldWidth(0);
    out<<endl;
    out.setFieldWidth(20);

    out<<"Estimated path time: " + QString::number(time_estimator.getPathTime());

    file.close();

    msg_box.setIcon(QMessageBox::Information);
    msg_box.setText("Saving the file has been succesful.");
    msg_box.exec();

    return;
}

void MainWindow::loadTableFromFile()
{
    QString filename = QFileDialog::getOpenFileName(this, tr("Open file with the path"), "/home", tr("Text Files (*.txt)"));
    QFile file(filename);

    if(!file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        msg_box.setIcon(QMessageBox::Critical);
        msg_box.setText("Could not open the file. Maybe it's the wrong name?");
        msg_box.exec();

        return;
    }

    time_estimator.clearActions();
    time_estimator.clearEvents();

    QTextStream in(&file);

    QString line = in.readLine();
    line = line.trimmed();
    QStringList list = line.split(",", QString::SkipEmptyParts, Qt::CaseInsensitive);

    int line_number = 1;
    bool conversion_ok;

    while(!line.isNull())
    {
        if(list.at(0) == "Straight - length")
        {
            double length = list.at(2).toDouble(&conversion_ok);
            if(conversion_ok)
            {
                time_estimator.addALineLength(length);
            }
            else
            {
                msg_box.setIcon(QMessageBox::Critical);
                msg_box.setText("Error in line: " + QString::number(line_number) + " in the column \"value\". It's not a number.");
                msg_box.exec();

                return;
            }
        }
        else if(list.at(0) == "Turn - radius")
        {
            QString tmp = list.at(1);
            tmp = tmp.trimmed();

            QStringList tmp2 = list.at(2).split("/");

            bool conversion_ok2;
            double corr_out = tmp2.at(0).toDouble(&conversion_ok);
            double corr_in = tmp2.at(1).toDouble(&conversion_ok2);
            if(conversion_ok && conversion_ok2)
            {
                if(tmp == "Right")
                {
                    time_estimator.addAArcRadius(corr_in, corr_out);
                }
                else if(tmp == "Left")
                {
                    time_estimator.addAArcRadius(corr_in, corr_out, false);
                }
                else
                {
                    msg_box.setIcon(QMessageBox::Critical);
                    msg_box.setText("Error in line: " + QString::number(line_number) + " in the column \"type\"");
                    msg_box.exec();

                    return;
                }
            }
            else
            {
                msg_box.setIcon(QMessageBox::Critical);
                msg_box.setText("Error in line: " + QString::number(line_number) + " in the column \"value\". It's not a number.");
                msg_box.exec();

                return;
            }
        }
        else if(list.at(0) == "Loading - time")
        {
            QString tmp = list.at(1);
            tmp = tmp.trimmed();

            double time = list.at(2).toDouble(&conversion_ok);
            if(conversion_ok)
            {
                if(tmp == "Load")
                {
                    time_estimator.addALoadingTime(time, 1.0);
                }
                else if(tmp == "Unload")
                {
                    time_estimator.addALoadingTime(time, -1.0);
                }
                else
                {
                    msg_box.setIcon(QMessageBox::Critical);
                    msg_box.setText("Error in line: " + QString::number(line_number) + " in the column \"type\"");
                    msg_box.exec();

                    return;
                }
            }
            else
            {
                msg_box.setIcon(QMessageBox::Critical);
                msg_box.setText("Error in line: " + QString::number(line_number) + " in the column \"value\". It's not a number.");
                msg_box.exec();

                return;
            }
        }
        else if(list.at(0) == "Stop - time")
        {
            double time = list.at(1).toDouble(&conversion_ok);
            if(conversion_ok)
            {
                time_estimator.addEWaitTime(time);
            }
            else
            {
                msg_box.setIcon(QMessageBox::Critical);
                msg_box.setText("Error in line: " + QString::number(line_number) + " in the column \"value\". It's not a number.");
                msg_box.exec();

                return;
            }
        }
        else if(list.at(0) == "Obstacle - radius")
        {
            double radius = list.at(1).toDouble(&conversion_ok);
            if(conversion_ok)
            {
                time_estimator.addEObstacleRadius(radius);
            }
            else
            {
                msg_box.setIcon(QMessageBox::Critical);
                msg_box.setText("Error in line: " + QString::number(line_number) + " in the column \"value\". It's not a number.");
                msg_box.exec();

                return;
            }
        }
        else if(list.at(0) == "ACTIONS" || list.at(0) == "Name of action" ||
                list.at(0) == "EVENTS" || list.at(0) == "Name of event" ||
                list.at(0).isEmpty())
        {

        }
        else if(list.at(0) == "END OF PATH")
        {
            break;
        }
        else
        {
            msg_box.setIcon(QMessageBox::Critical);
            msg_box.setText("Error in line: " + QString::number(line_number) + ".\nType of part not recognized. Maybe there's no colon after name of part?");
            msg_box.exec();

            return;
        }

        line = in.readLine();
        line = line.trimmed();
        list = line.split(",", QString::SkipEmptyParts, Qt::CaseInsensitive);
        line_number++;
    }

    countersUpdate();
    updateTableTrajectory();
    updateTableEvents();

    msg_box.setIcon(QMessageBox::Information);
    msg_box.setText("Loading the file has been succesful.");
    msg_box.exec();

    return;
}

//SLOTY*****************************************
//#########################PATH#################
//ODCINKI PROSTE--------------------------------
//dodawanie nowego odcinka
void MainWindow::on_pushButton_dod_odc_clicked()
{
    ui->pushButton_ok_dl_odc->setEnabled(true);
    ui->spinBox_length_line->setEnabled(true);
}

//ustawienie dlugosci odcinka
void MainWindow::on_pushButton_ok_dl_odc_clicked()
{            
    if(ui->spinBox_length_line->value()!=0)
    {
        time_estimator.addALineLength(ui->spinBox_length_line->value(),number_of_action_row);

        ui->pushButton_ok_dl_odc->setEnabled(false);
        ui->spinBox_length_line->setEnabled(false);

        updateTableTrajectory();

        number_of_action_row = -1;    
    }

    countersUpdate();
}

//ZAKRETY----------------------------------------
//dodawanie nowego zakretu
void MainWindow::on_pushButton_dod_zak_clicked()
{
    ui->pushButton_ok_pr_zak->setEnabled(true);
    ui->spinBox_cor_in->setEnabled(true);
    ui->SpinBox_cor_out->setEnabled(true);
    ui->comboBox_right_left->setEnabled(true);
}

//ustawienie promienia zakretu
void MainWindow::on_pushButton_ok_pr_zak_clicked()
{
    if(ui->spinBox_cor_in->value()!=0 && ui->SpinBox_cor_out->value() !=0)
    {
        bool is_right;

        if(ui->comboBox_right_left->currentText()=="Right")
        {
            is_right = true;
        }
        else
        {
            is_right = false;
        }

        time_estimator.addAArcRadius(ui->spinBox_cor_in->value(),ui->SpinBox_cor_out->value(),is_right,number_of_action_row);

        ui->pushButton_ok_pr_zak->setEnabled(false);
        ui->spinBox_cor_in->setEnabled(false);
        ui->SpinBox_cor_out->setEnabled(false);
        ui->comboBox_right_left->setEnabled(false);

        updateTableTrajectory();

        number_of_action_row = -1;
    }

    countersUpdate();
}

//LOADING/UNLOADING--------------------------------
//adding new load
void MainWindow::on_pushButton_dod_load_clicked()
{
    ui->pushButton_ok_time_load->setEnabled(true);
    ui->spinBox_time_load->setEnabled(true);
    ui->comboBox_load_unload->setEnabled(true);
}

//setting time of load
void MainWindow::on_pushButton_ok_time_load_clicked()
{
    if(ui->spinBox_time_load->value()!=0)
    {
        double load = 0;

        if(ui->comboBox_load_unload->currentText()=="Load")
        {
            load = 1;
        }
        if(ui->comboBox_load_unload->currentText()=="Unload")
        {
            load = -1;
        }

        time_estimator.addALoadingTime(ui->spinBox_time_load->value(), load, number_of_action_row);

        ui->pushButton_ok_time_load->setEnabled(false);
        ui->spinBox_time_load->setEnabled(false);
        ui->comboBox_load_unload->setEnabled(false);

        updateTableTrajectory();

        number_of_action_row = -1;
    }

    countersUpdate();
}

//#########################EVENTS#################
//ZATRZYMANIE--------------------------------------
//dodanie nowego zatrzymania
void MainWindow::on_pushButton_dod_zat_clicked()
{
    ui->pushButton_ok_czas_zat->setEnabled(true);
    ui->spinBox_time_stop->setEnabled(true);
}

//ustawienie czasu zatrzymania
void MainWindow::on_pushButton_ok_czas_zat_clicked()
{
    time_estimator.addEWaitTime(ui->spinBox_time_stop->value());

    ui->pushButton_ok_czas_zat->setEnabled(false);
    ui->spinBox_time_stop->setEnabled(false);

    updateTableEvents();

    countersUpdate();
}

//PRZESZKODY----------------------------------------
//dodawanie nowej przeszkody
void MainWindow::on_pushButton_dod_prze_clicked()
{
    ui->pushButton_ok_pr_prze->setEnabled(true);
    ui->comboBox_obstacle_size->setEnabled(true);
    ui->pushButton_advanced_obstacles->setEnabled(true);
}

//ustawienie czasu zatrzymania
void MainWindow::on_pushButton_ok_pr_prze_clicked()
{
    double radius = 0;

    if(ui->comboBox_obstacle_size->currentText() == "Small")
    {
        radius = 0.18;
    }

    if(ui->comboBox_obstacle_size->currentText() == "Medium")
    {
        radius = 0.5;
    }

    if(ui->comboBox_obstacle_size->currentText() == "Large")
    {
        radius = 1;
    }

    time_estimator.addEObstacleRadius(radius);

    ui->pushButton_ok_pr_prze->setEnabled(false);
    ui->comboBox_obstacle_size->setEnabled(false);
    ui->pushButton_advanced_obstacles->setEnabled(false);

    updateTableEvents();

    countersUpdate();
}

void MainWindow::on_pushButton_advanced_obstacles_clicked()
{
    CAdvancedObstacles advanced;
    //advanced.exec();

    double radius = 0;

    if(advanced.exec())
    {
        if(advanced.getShape() == "Circle")
        {
            radius = advanced.getDimension1();
        }

        if(advanced.getShape() == "Square")
        {
            radius = (sqrt(2)*advanced.getDimension1())/2;
        }

        if(advanced.getShape() == "Rectangle")
        {
            radius = (sqrt(pow(advanced.getDimension1(),2) + pow(advanced.getDimension2(),2)))/2;
        }
    }

    if(radius != 0)
    {
        time_estimator.addEObstacleRadius(radius);
    }

    ui->pushButton_ok_pr_prze->setEnabled(false);
    ui->comboBox_obstacle_size->setEnabled(false);
    ui->pushButton_advanced_obstacles->setEnabled(false);

    updateTableEvents();

    countersUpdate();
}

//------------------------------------------------
//OBLICZANIE
void MainWindow::on_pushButton_next_clicked()
{
    //writeTableToFile();
    CDialog dialog;
    dialog.setTimeValue(time_estimator.getPathTime());
    dialog.setActionsToVisualization(time_estimator.getActions());
    dialog.paint();
    dialog.exec();
}

//Obsluga tabeli sciezki
void MainWindow::on_pushButton_clear_path_clicked()
{
    time_estimator.clearActions();

    clearTable(ui->table_trajectory);
}

void MainWindow::on_pushButton_add_to_path_clicked()
{
    number_of_action_row = ui->table_trajectory->currentRow() + 1;
}

void MainWindow::on_pushButton_delete_from_path_clicked()
{
    if(ui->table_trajectory->currentRow() != -1)
    {
        if(ui->table_trajectory->rowCount() != 0)
        {
            time_estimator.deleteAction(ui->table_trajectory->currentRow());
        }

        ui->table_trajectory->removeRow(ui->table_trajectory->currentRow());
    }

    countersUpdate();
}

//Obsluga tabeli zdarzen
void MainWindow::on_pushButton_clear_events_clicked()
{
    time_estimator.clearEvents();

    clearTable(ui->table_events);
}

void MainWindow::on_pushButton_delete_from_event_clicked()
{
    if(ui->table_events->currentRow() != -1)
    {
        if(ui->table_events->rowCount() != 0)
        {
            time_estimator.deleteEvent(ui->table_events->currentRow());
        }

        ui->table_events->removeRow(ui->table_events->currentRow());
    }

    countersUpdate();
}
