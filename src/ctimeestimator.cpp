#include "headers/ctimeestimator.h"

CTimeEstimator::CTimeEstimator()
{

}

double CTimeEstimator::getPathTime()
{
    if(actions.size() == 0)
    {
        return 0;
    }
    else
    {
        actions.back()->addStartOrStop(Stop);

        return countActionsTime() + countEventsTime();
    }
}

void CTimeEstimator::clearActions()
{
    actions.clear();
}

void CTimeEstimator::clearEvents()
{
    events.clear();
}

void CTimeEstimator::addALineLength(double length, int position)
{
    addStraigthOrTurn(StraigthLine, length, true, position);
}

void CTimeEstimator::addAArcRadius(double cor_in_w, double cor_out_w, bool is_right, int position)
{
    double radius = (cor_in_w + cor_out_w)/2;
    addStraigthOrTurn(Turn, radius, is_right, position, cor_in_w, cor_out_w);
}

void CTimeEstimator::addALoadingTime(double time, double load, int position)
{
    CALoading *loading = new CALoading(&robot, time, load);

    vector<CAction *>::iterator it;
    bool empty = actions.empty();

    if(position == -1)
    {
        it = actions.end();
        position = actions.size();
    }
    else
    {
        it = actions.begin() + position;
    }

    if(it == actions.begin())
    {
        if(!empty)
        {
            actions.at(position)->addStartOrStop(Start);
        }
    }
    else
    {
        if(it == actions.end())
        {
            if(!empty)
            {
                actions.at(position - 1)->addStartOrStop(Stop);
            }
        }
        else
        {
            actions.at(position)->addStartOrStop(Start);
            actions.at(position - 1)->addStartOrStop(Stop);
        }
    }

    if(it == actions.end())
    {
        actions.push_back(loading);
    }
    else
    {
        actions.insert(it, loading);
    }
}

void CTimeEstimator::deleteAction(int position)
{
    vector<CAction *>::iterator it;

    if(position == -1)
    {
        it = actions.end();
        position = actions.size() - 1;
    }
    else
    {
        it = actions.begin() + position;
    }

    if(it == actions.begin())
    {
        actions.erase(it);
        actions.front()->addStartOrStop(Start);
    }
    else
    {
        if(it == actions.end() - 1)
        {
            actions.erase(it);
            actions.back()->addStartOrStop(Stop);
        }
        else
        {
            if(checkTypeOfAction(actions.at(position)) == Loading)
            {
                if(checkTypeOfAction(actions.at(position + 1)) != Loading)
                {
                    actions.at(position - 1)->removeStartOrStop(Stop);
                    actions.at(position + 1)->removeStartOrStop(Start);
                }
            }

            if(checkTypeOfAction(actions.at(position -1)) == Loading)
            {
                actions.at(position + 1)->addStartOrStop(Start);
            }

            if(checkTypeOfAction(actions.at(position + 1)) == Loading)
            {
                actions.at(position - 1)->addStartOrStop(Stop);
            }

            actions.erase(it);
        }
    }
}

void CTimeEstimator::deleteEvent(int position)
{
    vector<CEvent *>::iterator it;

    it = events.begin() + position;

    events.erase(it);
}

void CTimeEstimator::addEWaitTime(double time)
{
    CEWait *wait = new CEWait(time,&robot);
    events.push_back(wait);
}

void CTimeEstimator::addEObstacleRadius(double radius)
{
    CEObstacle *obstacle = new CEObstacle(radius,&robot);
    events.push_back(obstacle);
}

vector<CAction *> CTimeEstimator::getActions()
{
    return actions;
}

vector<CEvent *> CTimeEstimator::getEvents()
{
    return events;
}

double CTimeEstimator::countActionsTime()
{
    double time = 0;

    for(unsigned i = 0; i < actions.size(); i++)
    {
        CAction *action = actions.at(i);
        time += action->countTimeOfPart();
    }

    return time;
}

double CTimeEstimator::countEventsTime()
{
    double time = 0;

    for(unsigned i = 0; i < events.size(); i++)
    {
        time += events.at(i)->countTimeOfPart();
    }

    return time;
}

int CTimeEstimator::checkTypeOfAction(CAction *action)
{
    CAStraigthLine *line = dynamic_cast<CAStraigthLine *>(action);
    CATurn *turn = dynamic_cast<CATurn *>(action);
    CALoading *loading = dynamic_cast<CALoading *>(action);

    if(line)
    {
        return StraigthLine;
    }

    if(turn)
    {
        return Turn;
    }

    if(loading)
    {
        return Loading;
    }
}

int CTimeEstimator::checkTypeOfEvent(CEvent *event)
{
    CEWait *wait = dynamic_cast<CEWait *>(event);
    CEObstacle *obstacle = dynamic_cast<CEObstacle *>(event);

    if(wait)
    {
        return Wait;
    }

    if(obstacle)
    {
        return Obstacle;
    }
}

void CTimeEstimator::addStraigthOrTurn(int type, double value, bool is_right, int position, double corridor_in, double corrdor_out)
{
    CAction *action;

    if(type == StraigthLine)
    {
        action = new CAStraigthLine(&robot, value, NoStartStop);
    }
    if(type == Turn)
    {
        action = new CATurn(&robot, corridor_in, corrdor_out, NoStartStop, is_right);
    }

    vector<CAction *>::iterator it;

    if(position == -1)
    {
        it = actions.end();
        position = actions.size();
    }
    else
    {
        it = actions.begin() + position;
    }

    if(it == actions.begin())
    {
        action->addStartOrStop(Start);
    }
    else
    {
        if(checkTypeOfAction(actions.at(position - 1)) == Loading)
        {
            action->addStartOrStop(Start);
            if(it != actions.end())
            {
                actions.at(position)->removeStartOrStop(Start);
            }
        }
    }

    if(it == actions.end())
    {
        actions.push_back(action);
    }
    else
    {
        actions.insert(it, action);
    }
}

CRobot CTimeEstimator::getRobot()
{
    return robot;
}
