#include "headers/cabstractpart.h"

CAbstractPart::CAbstractPart(CRobot *robot_, double value)
{
    robot = robot_;

    value_of_part = value;
}

void CAbstractPart::setValueOfPart(double value)
{
    value_of_part = value;
}

double CAbstractPart::getValueOfPart() const
{
    return value_of_part;
}
