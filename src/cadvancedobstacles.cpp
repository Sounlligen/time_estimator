#include "headers/cadvancedobstacles.h"
#include "ui_cadvancedobstacles.h"

CAdvancedObstacles::CAdvancedObstacles(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CAdvancedObstacles)
{
    ui->setupUi(this);
    this->setWindowTitle("Advanced");

    ui->doubleSpinBox_dim1->setEnabled(false);
    ui->doubleSpinBox_dim2->setEnabled(false);
}

CAdvancedObstacles::~CAdvancedObstacles()
{
    delete ui;
}

void CAdvancedObstacles::on_comboBox_obstacle_shape_currentIndexChanged(const QString &arg1)
{
    if(arg1 == "Square" || arg1 == "Circle")
    {
        ui->doubleSpinBox_dim1->setEnabled(true);
        ui->doubleSpinBox_dim2->setEnabled(false);
    }

    if(arg1 == "Rectangle")
    {
        ui->doubleSpinBox_dim1->setEnabled(true);
        ui->doubleSpinBox_dim2->setEnabled(true);
    }

    if(arg1 == "-")
    {
        ui->doubleSpinBox_dim1->setEnabled(false);
        ui->doubleSpinBox_dim2->setEnabled(false);
    }
}

void CAdvancedObstacles::on_buttonBox_accepted()
{
    setDimension1(ui->doubleSpinBox_dim1->value());
    setDimension2(ui->doubleSpinBox_dim2->value());
    setShape(ui->comboBox_obstacle_shape->currentText());
}

QString CAdvancedObstacles::getShape() const
{
    return shape;
}

void CAdvancedObstacles::setShape(const QString &value)
{
    shape = value;
}

double CAdvancedObstacles::getDimension2() const
{
    return dimension2;
}

void CAdvancedObstacles::setDimension2(double value)
{
    dimension2 = value;
}

double CAdvancedObstacles::getDimension1() const
{
    return dimension1;
}

void CAdvancedObstacles::setDimension1(double value)
{
    dimension1 = value;
}
