#include "headers/crobot.h"

CRobot::CRobot()
{
    //Actually everything besides max_speed will be the same, for simplicity

    speed_ramp = new CSpeedRamp(0.35,0.8,0.17,0.655,0.06);

    robot_width = 0.402;
    avoidance_entry_radius = 0.25;
    avoidance_offset = 0.5;
}

void CRobot::setParams(double small_obstacle_radius_, double avoidance_offset_)
{
    avoidance_entry_radius = small_obstacle_radius_;
    avoidance_offset = avoidance_offset_;
}

void CRobot::setAvoidanceEntryRadius(double value)
{
    avoidance_entry_radius = value;
}

void CRobot::setAvoidanceOffset(double value)
{
    avoidance_offset = value;
}

double CRobot::getRobotWidth() const
{
    return robot_width;
}

double CRobot::getAvoidanceEntryRadius() const
{
    return avoidance_entry_radius;
}

double CRobot::getAvoidanceOffset() const
{
    return avoidance_offset;
}

CSpeedRamp *CRobot::getSpeedRamp() const
{
    return speed_ramp;
}

bool CRobot::getIsLoaded() const
{
    return is_loaded;
}

void CRobot::setIsLoaded(bool value)
{
    is_loaded = value;
}
