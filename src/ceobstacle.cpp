#include "headers/ceobstacle.h"

CEObstacle::CEObstacle(double value_of_event_, CRobot *robot_):
    CEvent(value_of_event_,robot_)
{

}

double CEObstacle::countTimeOfPart()
{
    double angle = M_PI/2;
    double angle_length_r = angle*robot->getAvoidanceEntryRadius();
    double angle_length_R = angle*(value_of_part + robot->getAvoidanceOffset());

    robot->getSpeedRamp()->countArcMaxSpeed(robot->getRobotWidth(), robot->getAvoidanceEntryRadius());
    double x1 = 2*(angle_length_r/robot->getSpeedRamp()->getArcMaxSpeed());

    robot->getSpeedRamp()->countArcMaxSpeed(robot->getRobotWidth(), robot->getAvoidanceOffset() + value_of_part);
    double x2 = 2*(angle_length_R/robot->getSpeedRamp()->getArcMaxSpeed());

    double x3 = 2*(robot->getAvoidanceEntryRadius() + robot->getAvoidanceOffset() + value_of_part)/robot->getSpeedRamp()->getMaxSpeed();

    return x1 + x2 - x3;
}
