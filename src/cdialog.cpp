#include "headers/cdialog.h"
#include "ui_cdialog.h"

CDialog::CDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CDialog)
{
    ui->setupUi(this);
    this->setWindowTitle("Result");

    //rysowanie animacji
    pixmapVisualization = new QPixmap(ui->visualization->size());
    pixmapVisualization->fill(QColor("grey"));
    painterVisualization = new QPainter(pixmapVisualization);
    ui->visualization->setPixmap(*pixmapVisualization);
}

void CDialog::setTimeValue(double time)
{
    ui->time_display->setText(QString::number(time, 'f', 1)+" s");
}

CDialog::~CDialog()
{
    delete ui;
}

//zamykanie
void CDialog::on_pushButton_close_clicked()
{
    CDialog::close();
}

void CDialog::setActionsToVisualization(vector<CAction *> value)
{
    actions_to_vis = value;
}

void CDialog::paint()
{
    painterVisualization->setPen(QPen(QBrush(Qt::black), 5));

    xp = 150;
    yp = 150;

    factor = 1;

    double max_length = 0;

    for(unsigned i = 0; i < actions_to_vis.size(); i++)
    {
        int action_type = CTimeEstimator::checkTypeOfAction(actions_to_vis.at(i));

        if(action_type == StraigthLine)
        {
            max_length += actions_to_vis.at(i)->getValueOfPart();
        }
    }

    factor = 150/(1.2*max_length); //scale factor

    int direction = Right;

    //setting start point
    QPoint p0(xp,yp);
    points.push_back(p0);

    //points creating from actions vector
    for(unsigned i = 0; i < actions_to_vis.size(); i++)
    {
        int action_type = CTimeEstimator::checkTypeOfAction(actions_to_vis.at(i));

        int x = points.back().x();
        int y = points.back().y();

        if(action_type == StraigthLine)
        {
            switch (direction) {
            case Right: x =  x + factor*actions_to_vis.at(i)->getValueOfPart();
                break;

            case Down: y = y + factor*actions_to_vis.at(i)->getValueOfPart();
                break;

            case Left: x = x - factor*actions_to_vis.at(i)->getValueOfPart();
                break;

            case Up: y = y - factor*actions_to_vis.at(i)->getValueOfPart();
                break;
            }
        }

        if(action_type == Turn)
        {
            CATurn *turn = dynamic_cast<CATurn *>(actions_to_vis.at(i));

            if(turn->getRight())
            {
                switch (direction) {
                case Right: direction = Down;
                    break;

                case Down: direction = Left;
                    break;

                case Left: direction = Up;
                    break;

                case Up: direction = Right;
                    break;
                }
            }
            else
            {
                switch (direction) {
                case Right: direction = Up;
                    break;

                case Down: direction = Right;
                    break;

                case Left: direction = Down;
                    break;

                case Up: direction = Left;
                    break;
                }
            }
        }

        if(action_type == Loading)
        {
            //x = points.back().x();
            //y = points.back().y();
            QPoint load(x,y);
            loadings.push_back(load);
        }

        QPoint p(x,y);
        points.push_back(p);
        painterVisualization->drawPoint(points.at(i));
    }

    //all points drawing
    for(unsigned i = 0; i < points.size(); i++)
    {
        painterVisualization->drawPoint(points.at(i));
    }

    //lines drawing
    for(unsigned i = 1; i < points.size(); i++)
    {
        painterVisualization->drawLine(points.at(i-1),points.at(i));
    }

    //loadings drawing
    painterVisualization->setPen(QPen(QBrush(Qt::yellow), 8));

    for(unsigned i = 0; i < loadings.size(); i++)
    {
        painterVisualization->drawPoint(loadings.at(i));
    }

    //pixmap update
    ui->visualization->setPixmap(*pixmapVisualization);
}
