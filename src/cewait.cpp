#include "headers/cewait.h"

CEWait::CEWait(double value_of_event_, CRobot *robot_):
    CEvent(value_of_event_,robot_)
{

}

double CEWait::countTimeOfPart()
{
    return value_of_part + robot->getSpeedRamp()->getAccTime() + robot->getSpeedRamp()->getDeccTime() - robot->getSpeedRamp()->getDeccAccDistMaxSpeed();
}
